import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { PrimerCompComponent } from './primerosComponentesTest/primer-comp/primer-comp.component';
import { TopBarComponent } from './TutorialGettingStarted/top-bar/top-bar.component';
import { ProductListComponent } from './TutorialGettingStarted/product-list/product-list.component';
import { ProductAlertComponent } from './TutorialGettingStarted/product-list/product-alert/product-alert.component';
import { PadreNumComponent } from './InputOutputOperaciones/padre-num/padre-num.component';
import { HijoOperacionesComponent } from './InputOutputOperaciones/padre-num/hijo-operaciones/hijo-operaciones.component';
import { FormsModule } from '@angular/forms';
import { PadreNombreComponent } from './InputOutput-1/padre-nombre/padre-nombre.component';
import { HijoNombreComponent } from './InputOutput-1/padre-nombre/hijo-nombre/hijo-nombre.component';

@NgModule({
  declarations: [
    AppComponent,
    PrimerCompComponent,
    TopBarComponent,
    ProductListComponent,
    ProductAlertComponent,
    PadreNumComponent,
    HijoOperacionesComponent,
    PadreNombreComponent,
    HijoNombreComponent
  ],
  imports: [
    BrowserModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
