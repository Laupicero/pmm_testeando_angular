// ARCHIVO README-EXPLICATIVO

Aquí dentro del componente 'padre-nombre', hemos anidado a un componente hijo 'hijo-nombre'.
Con ambos prácticaremos de primera mano la comunicación entre estos componentes

Para ello, utilizaremos los decoradores @Input() y @Output().
-Input: pasamos los datos del 'padre' al hijo
-Output: pasamos del 'hijo' al 'padre'

(Siempre pensamos desde el componente hijo)

//--------------
// INPUT
//--------------
1-Nos vamos al componente hijo, justo al archivo 'ts'
Y usaremos el decorador '@Input()' que hace que recibamos una propiedad de fuera.

2-Nos vamos al componente-padre y dentro de las etiquetas del hijo, 
escribimos el nombre de la variable (como se llama en el componente hijo)
Y lo que le vamos a pasar. EJEMPLO:
<!--Input-->
<hijo-nombre [nombreFromPadre]=nombre></hijo-nombre>


//--------------
// OUTPUT
//--------------
1-Con Output, hacemos exactamente lo contrario 'Emitimos un evento'.
Tendremos que declaralo así:
@Output() nombreFromHijo = new EventEmitter();

Los EventEmitter son genéricos, es decir, hay que especifiarle el tipo de dato que va a emitir.
(Aunque tampoco pasa nada por especificar)

2-Luego ejecutamos la emisión así: this.nombreFromHijo.emit(this.nombreFromPadre);

3-Lo recogemos desde el comonente padre, a través de la etiqueta del hijo:
<hijo-nombre (nombreFromHijo)="nombre = $event"></hijo-nombre>

4-Lo que hace es sustituir el contenido de 'nombre' (variable propia del padre) por la del event