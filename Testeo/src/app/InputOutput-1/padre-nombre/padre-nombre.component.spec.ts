import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PadreNombreComponent } from './padre-nombre.component';

describe('PadreNombreComponent', () => {
  let component: PadreNombreComponent;
  let fixture: ComponentFixture<PadreNombreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PadreNombreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PadreNombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
