import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'hijo-nombre',
  templateUrl: './hijo-nombre.component.html',
  styleUrls: ['./hijo-nombre.component.css']
})
export class HijoNombreComponent implements OnInit {
  @Input() nombreFromPadre: String;
  @Output() nombreFromHijo = new EventEmitter<String>();

  constructor() { }

  ngOnInit(): void {
    this.nombreFromPadre = `Sin nombre`;
  }

  //Métodos
  cambiarNombreFromHijo(){
    this.nombreFromPadre = `Paula Picero`;
    this.nombreFromHijo.emit(this.nombreFromPadre);
  }

}
