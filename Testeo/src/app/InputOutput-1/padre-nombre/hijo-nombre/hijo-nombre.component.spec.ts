import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HijoNombreComponent } from './hijo-nombre.component';

describe('HijoNombreComponent', () => {
  let component: HijoNombreComponent;
  let fixture: ComponentFixture<HijoNombreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HijoNombreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HijoNombreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
