import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'padre-test1',
  templateUrl: './padre-nombre.component.html',
  styleUrls: ['./padre-nombre.component.css']
})
export class PadreNombreComponent implements OnInit {
  nombre: String;

  constructor() { }

  ngOnInit(): void {
    this.nombre = `Nombre usuario`;
  }

  //Métodos
  cambiarNombre(){
    this.nombre = `Laura Picero`;
  }
}
