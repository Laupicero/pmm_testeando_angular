import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'primercomponente',
  templateUrl: './primer-comp.component.html',
  styleUrls: ['./primer-comp.component.css']
})
export class PrimerCompComponent implements OnInit {
  nombre: string = `Laura`;

  constructor() { 
    console.log("Mi primer componente cargado");
  }

  ngOnInit(): void {
  }

}
