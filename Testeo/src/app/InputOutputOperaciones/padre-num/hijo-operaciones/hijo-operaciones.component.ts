import { Component, OnInit, Input, Output, EventEmitter, AfterContentChecked } from '@angular/core';

@Component({
  selector: 'app-hijo-operaciones',
  templateUrl: './hijo-operaciones.component.html',
  styleUrls: ['./hijo-operaciones.component.css']
})
export class HijoOperacionesComponent implements OnInit {
  //El decorador 'Input' quiere decir que: Vas a recibir una propiedad de 'afuera' / De tu componente padre
  @Input() valor1: string;
  @Input() valor2: string;
  aux1: number;
  aux2: number;
  
  //Output: 'Emitimos' del componente hijo información al padre  
  @Output() envRes: EventEmitter<number> = new EventEmitter<number>();

  //Primero se ejecuta el constructor y luego en ngOnInit
  // Si sa cambia el contenido se pueden apreciar los fallos
  constructor() { }

  ngOnInit(): void {
    this.aux1 = parseFloat(this.valor1);
    this.aux2 = parseFloat(this.valor2);
  }

  //Operaciones
  suma(){  this.envRes.emit( this.aux1 +  this.aux2); } 
  resta(){  this.envRes.emit( this.aux1 -  this.aux2); }
  multiplica() {  this.envRes.emit( this.aux1 *  this.aux2); }
  divide(){  this.envRes.emit( this.aux1 /  this.aux2); }

}
