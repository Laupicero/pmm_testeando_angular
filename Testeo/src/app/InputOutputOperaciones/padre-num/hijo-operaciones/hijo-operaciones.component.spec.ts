import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HijoOperacionesComponent } from './hijo-operaciones.component';

describe('HijoOperacionesComponent', () => {
  let component: HijoOperacionesComponent;
  let fixture: ComponentFixture<HijoOperacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HijoOperacionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HijoOperacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
