import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre-num',
  templateUrl: './padre-num.component.html',
  styleUrls: ['./padre-num.component.css']
})
export class PadreNumComponent implements OnInit {
  valor1: string =  '10';
  valor2: string =  '20';
  resultadoP: number;

  captaResultado(event) {  this.resultadoP = event; } 

  constructor() { }

  ngOnInit(): void {}

}
