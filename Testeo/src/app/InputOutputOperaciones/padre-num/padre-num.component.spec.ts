import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PadreNumComponent } from './padre-num.component';

describe('PadreNumComponent', () => {
  let component: PadreNumComponent;
  let fixture: ComponentFixture<PadreNumComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PadreNumComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PadreNumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
